
module.exports = function(grunt) {

  grunt.initConfig({
    less: {
      style: {
      	files: {'style.css':'style.less'}
      }	
    },
    watch: {
      options: {
        atBegin: true,
        livereload: true
      },
      files: ['*.less'],
      tasks: ['less']
    },
    serve: {
        'path': 'Users/marco/hackathon/spark/'
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-serve');

  grunt.registerTask('default', ['jshint']);

};

